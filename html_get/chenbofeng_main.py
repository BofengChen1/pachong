# -*- coding: utf-8 -*-
import time
from html_get import initialization_information


recode_list = ['公司名称', '申请注册事项', '基金名称', '接收材料', '补正通知', '受理通知', '一次书面反馈', '行政许可决定书', 'page_seq']

url = "http://ndes.csrc.gov.cn/alappl/home/gongshi"

'''路径存放关键字为当前日期'''
initialization_path = 'news_' + time.strftime("%Y-%d-%m") + '.csv'
'''开始全量爬虫'''
initialization_information(recode_list=recode_list, url=url, initialization_path=initialization_path)


