# -*- coding: utf-8 -*-
import urllib2
from bs4 import BeautifulSoup
import sys
import re
from tqdm import tqdm
import pandas as pd

reload(sys)
sys.setdefaultencoding('utf-8')

'''发送连接网页的请求'''


def get_soup(url, timeout):

    request = urllib2.Request(url)
    response = urllib2.urlopen(url=request, timeout=timeout)
    contents = response.read()
    soup = BeautifulSoup(contents, "html.parser", from_encoding='utf-8')
    return soup

'''解析文本信息'''


def from_text_to_news(text, recode_dict):
    company = re.compile('^关于(.*?)的').findall(text)[0]
    key_new = re.compile('.*《(.*)》').findall(text)[0].split('—')
    event = key_new[0]
    if len(key_new) == 2:
        fund_name = key_new[1]
        recode_dict['基金名称'] = fund_name
    elif len(key_new) == 3 and key_new[1] == '':
        fund_name = key_new[2]
        recode_dict['基金名称'] = fund_name

    recode_dict['公司名称'] = company
    recode_dict['申请注册事项'] = event

    return recode_dict


''' 获取一页的所有信息 '''


def page_news(useful_url, page, timeout, page_news_list, recode_list):
    useful_url_page = useful_url + '&' + 'pageNo=' + str(page)
    soup_1 = get_soup(url=useful_url_page, timeout=timeout)

    for child in soup_1.find_all('div', attrs={'style': 'margin-top: -11px; margin-left: 12px;'}):
        recode_dict = {i: '' for i in recode_list}

        recode_dict['page_seq'] = page
        text = str(child.find('div', attrs={'class': 'titleshow'}).string[:])

        recode_dict = from_text_to_news(text, recode_dict=recode_dict)

        for child_0 in child.table.find_all('tr'):
            if child_0.attrs == {}:
                child_set = child_0.find_all('td')
                process_name = str([i for i in child_set[0].stripped_strings][0])
                process_time = [i for i in child_set[1].stripped_strings][0]
                recode_dict[process_name] = process_time
        page_news_list.append(recode_dict)
    return page_news_list

'''获取网站上所有页码的信息'''


def get_useful_news(url, timeout, part_name, recode_list):
    soup = get_soup(url=url, timeout=timeout)
    useful_url = soup.find('div', text=part_name).parent['href']
    # 获取总页数
    page_soup = get_soup(url=useful_url, timeout=timeout)
    page_num = int(re.findall(r"\d+\.?\d*", page_soup.find('span', attrs={'class': 'jump_text'}).string)[0])

    news_list = []
    for page in tqdm(xrange(1, page_num+1)):
        page_news_list = []
        page_news_list = page_news(useful_url=useful_url, page=page, timeout=timeout, page_news_list=page_news_list, recode_list=recode_list)
        news_list.extend(page_news_list)
    return news_list

'''初始化，全量爬虫'''


def initialization_information(recode_list, url, initialization_path):
    news_list = get_useful_news(url=url, timeout=100, part_name='机构部', recode_list=recode_list)
    news_pd = pd.DataFrame(map(lambda x: [x[i] for i in recode_list], news_list), columns=recode_list)
    news_pd.to_csv(initialization_path)
    return initialization_path

''' 增量运行，只爬虫top n pages 的信息'''


def get_top_n_page_news(top_page_num, initialization_path):
    return 0
