# -*- coding:utf8 -*-
import urllib
import re
from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.common.exceptions import TimeoutException, NoSuchElementException, WebDriverException
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import xlrd
import xlwt
import sys
import time
import os
import socket
socket.setdefaulttimeout(30.0) # urlretrieve默认超时退出时间

#处理页面标签类
class Tool:
	#去除img标签,7位长空格
	removeImg = re.compile('<img.*?>| {7}|')
	#删除超链接标签
	removeAddr = re.compile('<a.*?>|</a>')
	#把换行的标签换为\n
	replaceLine = re.compile('<tr>|<div>|</div>|</p>')
	#将表格制表<td>替换为\t
	replaceTD= re.compile('<td>')
	#把段落开头换为\n加空两格
	replacePara = re.compile('<p.*?>')
	#将换行符或双换行符替换为\n
	replaceBR = re.compile('<br><br>|<br>')
	#将其余标签剔除
	removeExtraTag = re.compile('<.*?>')
	def replace(self,x):
		x = re.sub(self.removeImg,"",x)
		x = re.sub(self.removeAddr,"",x)
		x = re.sub(self.replaceLine,"\n",x)
		x = re.sub(self.replaceTD,"\t",x)
		x = re.sub(self.replacePara,"\n    ",x)
		x = re.sub(self.replaceBR,"\n",x)
		x = re.sub(self.removeExtraTag,"",x)
		#strip()将前后多余内容删除
		return x.strip()
from remove_file import remove_img

class TAOBAO:
	# initial
	def __init__(self):
		self.user_agent = 'Mozilla/5.0 (Macintosh; U; Intel MacOS X10_6_8; en-us) AppleWebKit/534.50 (KHTML, like Gecko) Version/5.1 Safari/534/50'

		self.referer_page = 'https://s.taobao.com/search?ie=utf8&initiative_id=staobaoz_20170122&stats_click=search_radio_all%3A1&js=1&imgfile=&q=%E6%BB%B4%E9%9C%B2%E5%8F%A3%E7%BD%A9&suggest=0_2&_input_charset=utf-8&wq=%E6%BB%B4%E9%9C%B2&suggest_query=%E6%BB%B4%E9%9C%B2&source=suggest&fs=1&filter_tianmao=tmall&sort=sale-desc'	
		self.headers = { 'User-Agent': self.user_agent, 'Referer': self.referer_page}

		# test
		# self.start_url = 'https://detail.tmall.com/item.htm?spm=a230r.1.999.1.3064523cRw9R1Z&id=561335715503&ns=1&sku_properties=20509:28316' #冬季女韩版学生小个子毛呢外套女短款
		self.main_url = 'https://taobao.com'

		self.tool = Tool()
		self.driver = webdriver.Chrome()
		# self.driver = webdriver.Safari()

		self.load_path = '/Users/gdpdy/Desktop/round2_test/test_img/'
		# self.save_path = '/Users/louxiaojun/Desktop/test_img/'
		self.save_path = '/Users/gdpdy/Desktop/round2_test/pant_length_labels/'

	# wait mainpage load and upload imagetest
	def uploadImg(self,url,img_path):
		flag = False
		try:
			self.driver.get(url)
		except socket.timeout:
			print ('load main page time out')
			return flag

		try:
			WebDriverWait(self.driver,10).until(
				EC.presence_of_element_located((By.CLASS_NAME,"search-imgsearch-panel"))
			)
			print ('find main page search bar')
		# except TimeoutException:
		except:
			print ("time out, can't find main page search bar")
			return flag
		time.sleep(2)

		try:
			element = self.driver.find_element_by_id('J_IMGSeachUploadBtn')
			element.send_keys(img_path[0]+img_path[1])
			print ('upload image: %s'%(img_path[1]))
			return True
		except:
			print ('upload button is not found')
			return flag

	# get the first shop link
	def getShopLink(self):
		try:
			WebDriverWait(self.driver,10).until(
				EC.presence_of_element_located((By.ID,"imgsearch-banner"))
			)
			print ('find ID imgsearch-banner')
		# except TimeoutException:
		except:
			print ("time out, can't find ID imgsearch-banner")
			return None

		# check the category 
		try:
			element = self.driver.find_element_by_id('imgsearch-banner')
			subPageCode = element.get_attribute('innerHTML')
			soup = BeautifulSoup(subPageCode, 'lxml')
			item = soup.select('a[class="J_Ajax cat-item cat-item-selected"]')
			if len(item) == 0:
				print ('selected category not found')
				return None
			# 当neck_design_labels时，只选择上衣或者裙子或者内衣
			# elif not (item[0].get('trace-imgcat') == 'coat' or item[0].get('trace-imgcat') == 'skirt' or item[0].get('data-value')=='21'):
			# 	print ('category is %s, not correct'%(item[0].get('trace-imgcat')))
			# 	return None
			# # 当pant_length_labels时，只选择下装
			# elif not (item[0].get('trace-imgcat') == 'xiazhuang'):
			# 	print ('category is %s, not correct'%(item[0].get('trace-imgcat')))
			# 	return None
			else:
				print ('category is %s'%(item[0].get('trace-imgcat')))
		except:
			print ("time out, can't find category banner")
			return None

		try:
			element = self.driver.find_element_by_id('imgsearch-itemlist')
			subPageCode = element.get_attribute('innerHTML')
			soup = BeautifulSoup(subPageCode, 'lxml')
			item = soup.select('a[class="pic-link J_ItemPicA"]')
			link_address = []
			if len(item)>1:
				print ('shop is found')
				link_address1 = item[0].get('href')
				if link_address1.startswith('http'):
					link_address.append(link_address1)
				else:
					link_address.append('https:'+link_address1)
				link_address2 = item[1].get('href')
				if link_address2.startswith('http'):
					link_address.append(link_address2)
				else:
					link_address.append('https:'+link_address2)
				return link_address
			elif len(item)==1:
				print ('shop is found')
				link_address1 = item[0].get('href')
				if link_address1.startswith('http'):
					link_address.append(link_address1)
				else:
					link_address.append('https:'+link_address1)
				return link_address
			else:
				print ('no shop is found')
				return None
		except:
			return None

	def getImgLink(self):
		try:
			WebDriverWait(self.driver,10).until(
				EC.presence_of_element_located((By.ID,"J_UlThumb"))
			)
			print ('find ID J_UlThumb')
		# except TimeoutException:
		except:
			print ("time out, can't find ID J_UlThumb")
			return None

		
		# click the second img
		try:
			# element = self.driver.find_element_by_xpath("//ul[@id='J_UlThumb']/li[2]")
			# # print (element)
			# ActionChains(self.driver).move_to_element(element).perform()
			# time.sleep(1)

			pageCode = self.driver.page_source
			soup = BeautifulSoup(pageCode, 'lxml')
			item = soup.select('#J_ImgBooth')
			if len(item)>0:
				print ('Img link is found')
				link_address = item[0].get('src')
				print (link_address)
				if link_address.startswith('http'):
					return link_address
				else:
					link_address = 'https:'+link_address
					return link_address
			else:
				print ('Img link is not found')
				return None
		except:	
			print ('Mouse move to the certain img failed')
			return None

	# enter the shop page and download the image
	def getShopPage(self, url, img_path, shop_count):
		print ('enter the shop')
		try:
			self.driver.get(url)
		except socket.timeout:
			print ('load shop page time out')
			return 

		link_address = self.getImgLink()
		if link_address is None:
			return
		else:
			# download the img
			try:
				# if img is webp, skip
				if link_address.endswith('.jpg'):
					# urllib.request.urlretrieve(link_address,self.save_path+os.path.splitext(img_path[1])[0]+'_aug_'+str(shop_count)+'.jpg')
					urllib.request.urlretrieve(link_address,self.save_path+os.path.splitext(img_path[1])[0]+'.jpg')
					print ('Img download successed')
			except socket.timeout:
				print ('Img download failed')


	def downloadOnce(self,img_path):
		upload_flag = self.uploadImg(self.main_url, img_path)
		if upload_flag == False:
			return

		shop_link = self.getShopLink()
		print (shop_link)
		if shop_link is None:
			return
		else:
			for count,link in enumerate(shop_link):
				# if count == 0:
				# 	continue
				self.getShopPage(link, img_path, count)
				# break if only one img needed
				break

	def getFilePath(self,file_dir):   
		L=[]
		for root, dirs, files in os.walk(file_dir):  
			for file in files:  
				if os.path.splitext(file)[1] == '.jpg':  
					# L.append(os.path.join(root, file))  
					L.append([root,file])
		return L  

	def start(self):
		remove_img()

		images_path = self.getFilePath(self.load_path)
		# images_path = sorted(images_path, key = lambda x:x[1])
		# print (images_path[0])

		# try:
		# 	self.driver.get(self.main_url)
		# 	print ('sleep 30s!')
		# 	time.sleep(30)
		# except socket.timeout:
		# 	print ('load main page time out')
		# 	return flag

		# self.login_taobao()
		# self.driver.get("https://login.taobao.com/member/login.jhtml")
		print ('sleep 5s!')
		time.sleep(5)

		for count,p in enumerate(images_path):
			print ('##########   start download image: ', count, '   ##########')
			self.downloadOnce(p)
			if count > 20:
				break
			time.sleep(2)

		self.driver.quit()

	def login_taobao(self):
		self.driver.maximize_window() #将浏览器最大化显示
		self.driver.delete_all_cookies()
		self.driver.get("https://login.taobao.com/member/login.jhtml")
		#load the switch
		element=WebDriverWait(self.driver,60).until(lambda driver :
		self.driver.find_element_by_xpath("//*[@id='J_Quick2Static']"))
		element.click()
		self.driver.implicitly_wait(20)
		username=self.driver.find_element_by_name("TPL_username")
		if not username.is_displayed:
			self.driver.implicitly_wait(20)
			self.driver.find_element_by_xpath("//*[@id='J_Quick2Static']").click()
		self.driver.implicitly_wait(20)
		username.send_keys("xxx")
		sleep
		self.driver.find_element_by_name("TPL_password").send_keys("xxx")
		self.driver.implicitly_wait(20)
		self.driver.find_element_by_xpath("//*[@id='J_SubmitStatic']").click()
		time.sleep(4)
		# time.sleep(5)
		# cookie = [item["name"] + "=" + item["value"] for item in driver.get_cookies()]
		# cookiestr = ';'.join(item for item in cookie)
		# return driver

spider = TAOBAO()
spider.start()


